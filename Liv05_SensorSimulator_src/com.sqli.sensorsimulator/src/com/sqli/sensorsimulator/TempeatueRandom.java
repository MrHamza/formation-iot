package com.sqli.sensorsimulator;

import java.util.Random;

/**
 * Generateur de la temperature aleatoire
 * @author HSAIN
 */
public class TempeatueRandom {
	
	private Integer valeurReleve = null;
	
	private int intervalMin;
	
	private int intervalMax;
	
	private int currentIntervalMax;
	
	private int currentIntervalMin;
	
	private int pasInteval;

	public TempeatueRandom(int intervalMin, int intervalMax, int pasInteval) {
		super();
		this.intervalMin = intervalMin;
		this.intervalMax = intervalMax;
		this.pasInteval = pasInteval;
	}
	
	
	public int getNextTemperature() {
		if(valeurReleve == null) {
			valeurReleve = intervalMin + new Random().nextInt(intervalMax - intervalMin);
			currentIntervalMin = valeurReleve - pasInteval ;
			currentIntervalMax = valeurReleve + pasInteval ;
		}
		else {
			valeurReleve = currentIntervalMin + new Random().nextInt(currentIntervalMax - currentIntervalMin);
			currentIntervalMin = valeurReleve - pasInteval ;
			currentIntervalMax = valeurReleve + pasInteval ;
		}
		
                
                if (valeurReleve < intervalMin) {
                    valeurReleve = intervalMin + new Random().nextInt(intervalMax - intervalMin);
                    currentIntervalMin = valeurReleve - pasInteval ;
                    currentIntervalMax = valeurReleve + pasInteval ;
                }
		return valeurReleve.intValue();
	}
	
	

}
