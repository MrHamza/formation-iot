package com.sqli.sensorsimulator;

import com.sqli.sensorsimulator.gui.MainWindow;
import java.net.*;
import java.io.*;

/**
 * Thread associe � un capteur connect�
 * @author HSAIN
 */
public class Client extends Thread {
    
    public static int SEND_DELAI = 1000 ; 
    private Socket client;
    private String serveur;
    private TempeatueRandom tempeatueRandom;
    private boolean isRandom = true;
    private float currentTemperature = 10;
    private String idSite;
    private String idCapteur;
    //***********************************

    //les flux.....................
    private ObjectOutputStream sortie;
    //Interface graphique
    private MainWindow window;

    /**
     * Creer une nouvelle instance du simulateur (capteur)
     */
    public Client(String hote, MainWindow win) {
        this.serveur = hote;
        tempeatueRandom = new TempeatueRandom(0, 100, 5);
        window = win;
    }

    @Override
    public void run() {
        try {
            //Etape n:1;
            seConnecterAuServeur();
            //Etape n2;
            obtenirFlux();
            //Etape n3;
            traiterConnexion();
            //Etape n4;
            fermerConnexion();
        } catch (EOFException eof) {
            // le serveur a fermé la connexion
        } catch (IOException e) {

        }

    }

  

    //*****************************************************************************
    /**
     * Etablir le connexion avec le raspberry
     * @throws IOException 
     */
    public void seConnecterAuServeur() throws IOException {
        System.out.println("Connexion En cours...\n");
        // créer lengthsocket pour mettre en place la connexion au serveur .
        client = new Socket(InetAddress.getByName(this.serveur), 5001);
        System.out.println("Connecté à:" + client.getInetAddress().getHostName());
        
        //Afficher les informations sur l'interface graphique 
        window.setIdCapteur(InetAddress.getLocalHost().getHostAddress());
        window.setIdSite(client.getInetAddress().getHostAddress());
    }

    public String getIdSite() {
        return this.idSite;
    }

    public String getIdCapteur() {
        return this.idCapteur;
    }
//*****************************************************************************
    
    /**
     * Recuperer le output stream du raspberry pour l'envoi des messages (temperatures) 
     * @throws IOException 
     */
    public void obtenirFlux() throws IOException {
        this.sortie = new ObjectOutputStream(this.client.getOutputStream());
        sortie.flush();
        System.out.println("flux recu");
    }

//*****************************************************************************
    /**
     * Envoyer une temperature cahque SEND_DELAI ms 
     * @throws IOException 
     */    
    public void traiterConnexion() throws IOException {

        do {
            try {
                envoyerDonnee(genererTemperature());
                Thread.sleep(SEND_DELAI);
            } catch (InterruptedException ex) {
                System.err.println("Connexion perdue");
            }
        } while (true);

    }

//*****************************************************************************
    /**
     * Fermeture de la connexion 
     * @throws IOException 
     */
    public void fermerConnexion() throws IOException {
        System.out.println("Fermeture de la connexion");
        sortie.close();
        client.close();
        System.out.println("Connexion fermé");

    }
    //*****************************************************************************
    /**
     * Ecrire la temperature sur le flux de sortie 
     * @param temp 
     */
    public void envoyerDonnee(float temp) {
        System.out.println(temp + " C");
        window.setCurrentTemperature(temp);

        try {
            sortie.writeFloat(temp);

            sortie.flush();

        } catch (IOException e) {
            System.err.println("Connexion perdue!");
            try {
                fermerConnexion();
            } catch (IOException ex) {
                System.err.println("Erreur lors de la fermeture du flux");
            }

        }

    }


    /**
     * Générer une temperature aléatoire 
     * @return : la valeur de la temperature
     */
    private float genererTemperature() {
        if (isRandom) {
            currentTemperature = tempeatueRandom.getNextTemperature();
        }
        return currentTemperature;
    }
    
    /**
     * Activer/Desactiver la génération aléatoire de la température 
     * @param israndom 
     */
    public void setRandom(boolean israndom) {
        isRandom = israndom;
    }
    
    public float getCurrentTemperature() {
        return this.currentTemperature;
    }

    public void setCurrentTemperature(float current) {
        this.currentTemperature = current;
    }

}
