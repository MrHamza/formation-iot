package com.sqli.TempSens.response;

public class Response {
	
	public static final int SUCCES = 500 ; 
	public static final int FAILURE = -1 ; 
	
	public static final String MSG_ADD_SUCCES = "Ajout� avec succes";
	public static final String MSG_GET_SUCCES = "Donn�es recup�r�es avec succes";
	public static final String MSG_DEL_SUCCES = "Supprim� avec succes";
	public static final String MSG_UP_SUCCES = "Modifi� avec succes";
	public static final String MSG_ADD_FAILURE = "Erreur lors de l'ajout";
	public static final String MSG_DEL_FAILURE = "Erreur lors de la suppression";
	public static final String MSG_UP_FAILURE = "Erreur lors de la mise � jours";
	public static final String MSG_GET_FAILURE = "Erreur lors de la recuperation des donn�es";
	
	private int status ; 
	private Object obj ; 
	private String msg ; 
	
	public Response(){
		
	}

	/* SETTERS ET GETTERS */
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	
}
