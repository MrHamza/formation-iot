package com.sqli.TempSens.mqtt;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.sqli.TempSens.dao.CapteurDao;
import com.sqli.TempSens.dao.ReleveDao;
import com.sqli.TempSens.entities.Capteur;
import com.sqli.TempSens.entities.Releve;
import com.sqli.TempSens.entities.Site;
/**
 *
 * @author HSAIN
 */

public class MqttMessageHandler implements MqttCallback{

	
    private ReleveDao releveDao;
	private int currentId = 0 ; 
	private CapteurDao capteurDao ; 
	private Map<String,Date> mapUpdateDates ; 
	private MqttClient client ;
	private String brokerAddr ;  
	private String port ;  
	private String topic ;
	Calendar cal = Calendar.getInstance(); 


	public void setBrokerAddr(String brokerAddr) {
		this.brokerAddr = brokerAddr;
	}
	
	
	public void setPort(String port) {
		this.port = port;
	}
	
	
	public void setTopic(String topic) {
		this.topic = topic;
	}

 
   public MqttMessageHandler()
    {
       capteurDao = new CapteurDao() ; 
       releveDao = new ReleveDao() ; 
       mapUpdateDates = new HashMap<String, Date>();
    }
   
    
    public void lancerCient() throws UnknownHostException
    {
          try {
              //Initialisation des parametres 
            String clientId = "cli:"+InetAddress.getLocalHost().getHostAddress(); 
            MemoryPersistence persistence = new MemoryPersistence();
            client = new MqttClient("tcp://"+brokerAddr+":"+port, clientId, persistence);
            MqttConnectOptions opts = new MqttConnectOptions();
            opts.setCleanSession(true);
           
            //Connexion au broker 
            client.connect(opts);
            client.setCallback(this);
            //Inscription au topic 
            System.out.println("Subscription !! ================= =========================== =");
            client.subscribe(topic);
            System.out.println("Subsribed !! ================= =========================== =");
           
           
          } catch (MqttException e) {
        	  System.out.println("Erreur : "+e.getMessage());
                e.printStackTrace();
            }
    }
    @Override
    public void connectionLost(Throwable thrwbl) {
        System.out.println("Connexion perdue : "+thrwbl.getLocalizedMessage());
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        System.out.println("Nouveau message (Insertion dans la BD )");
        String[] tMsg = message.toString().split(":");
        if(tMsg.length == 2)
        {
        	
        	String idCapteur = tMsg[0]; 
        	float temperature = Float.parseFloat(tMsg[1]); 
        	
        	
            
            Capteur c = new Capteur(); 
            c.setIdCapteur(idCapteur);
            
            if(capteurDao == null )
            	System.out.println("CapteurManager == null ");
            
            
            c= capteurDao.getCapteurDetails(c.getIdCapteur()); 
            if(c != null)
            {
            	
            	Releve r = new Releve(); 
                r.setDateReleve(new Date());
                r.setTemperature(temperature);
                r.setIdCapteur(idCapteur);
            	Site s = c.getSiteCapteur();
            	r.setIdSite(s.getIdSite());
            	r.setLatCapteur(c.getLatCapteur());
                r.setLatSite(s.getLatSite());
                r.setLibelleCapteur(c.getLabel());
                r.setLibelleSite(s.getLibelleSite());
                r.setLngCapteur(c.getLngCapteur());
                r.setLngSite(s.getLngSite());
                r.setSeuilMax(c.getSeuilMax());
                r.setSeuilMin(c.getSeuilMin());
                releveDao.insertReleve(r);
                
                if(mapUpdateDates.containsKey(c.getIdCapteur()))
                {
                	System.out.println("Un capteur qui existe deja !");
                	Date lastMaximoUpdate = mapUpdateDates.get(c.getIdCapteur());
                	
                	Date currentDate = new Date();
                	
                	// Si la temperature est urgente 
                    if(r.getTemperature() > r.getSeuilMax() || r.getTemperature()<r.getSeuilMin())
                    {
                    	
                	    cal.setTime(lastMaximoUpdate); 
                	    cal.add(Calendar.MINUTE, 30); 
                	    
                    	//Mise a jours MAXIMO
                    	if(currentDate.after(cal.getTime()))
                    	{
                    		System.out.println("Insertion d'une alerte Maximo");
                    		sendReleveToMaximo(r);
                    		mapUpdateDates.put(c.getIdCapteur(), new Date()); 
                    	}
                    	else
                    	{
                    		System.out.println("Delai non atteind ! Insertion dans Maximo annul�e");
                    	}
                    }
                }
                else
                {
                	System.out.println("!Nouveau capteur ajout� a la liste");
                	mapUpdateDates.put(c.getIdCapteur(), new Date()); 
                	System.out.println("Insertion d'une alerte Maximo");
                	sendReleveToMaximo(r);
                }
                
            }
            else 
            {
            	System.out.println("Capteur inconnu ! ");
            }
           
            
        }
        
    }
    
    private void sendReleveToMaximo(Releve r) throws IOException
    {
    	Date currentDate = new Date(); 
    	String uri =
    		    "http://maximoworkshop.sqli.com/maxrest/rest/mbo/workorder/?&_lid=maxadmin&_lpwd=maxadmin&wonum=SQLISens-"+(currentId++)+"&siteid=1ETG";
    	GET(uri, r);
    	
    	System.out.println(uri);
    }
    
    private  String GET(String url , Releve r){
        InputStream inputStream = null;
        String result = "";
        try {
 
            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();
 
            HttpPost request = new HttpPost(url) ; 
            request.addHeader("Accept", "application/xml");
            
            
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            String desc = "--"; 
            if(r.getSeuilMax() < r.getTemperature())
            	desc = "["+r.getTemperature()+"C] superieure au seuil ["+r.getSeuilMin()+"]-Capteur :"+r.getLibelleCapteur()+"["+r.getIdCapteur()+"]";
            else if(r.getSeuilMin()> r.getTemperature())
            	desc = "["+r.getTemperature()+"C] inf�rieure au seuil  ["+r.getSeuilMax()+"]-Capteur :"+r.getLibelleCapteur()+"["+r.getIdCapteur()+"]";
    	    
            System.out.println(desc);
            urlParameters.add(new BasicNameValuePair("DESCRIPTION", desc));

            request.setEntity(new UrlEncodedFormEntity(urlParameters));
            
            HttpResponse httpResponse = httpclient.execute(request);
            
            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();
 
            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "ne marche pas!";
            
            System.out.println("APACHE REPONSE \n "+result);
 
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
 
        return result;
    }
 
    private String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
 
        inputStream.close();
        return result;
 
    }
    
    

    @Override
    public void deliveryComplete(IMqttDeliveryToken imdt) {
         System.out.println("DeliveryComplete");
    }
    
}
