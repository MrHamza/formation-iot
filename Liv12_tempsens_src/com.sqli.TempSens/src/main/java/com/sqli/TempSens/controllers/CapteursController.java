package com.sqli.TempSens.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sqli.TempSens.entities.Capteur;
import com.sqli.TempSens.entities.Validators;
import com.sqli.TempSens.managers.CapteurManager;
import com.sqli.TempSens.managers.SiteManager;
import com.sqli.TempSens.response.Response;

@Controller
@RequestMapping(value="/capteur")
public class CapteursController {

	@Autowired
	private CapteurManager capteurManager ; 
	
	@Autowired
	private SiteManager siteManager ; 
	
	
	@RequestMapping(value="/list")
	public String listCapteurs(Model m)
	{
		List<Capteur> l = capteurManager.getAllCapteurs() ; 
		m.addAttribute("capteurs", l); 
		return "capteur/list" ;
	}
	
	@RequestMapping(value="/add" , method=RequestMethod.GET)
	public String formAddCapteurs(Model m)
	{
		m.addAttribute("sites",siteManager.getAllSites()); 
		Capteur c = new Capteur(); 
		c.setLatCapteur(34.68472715484425);
		c.setLngCapteur(-1.9142245833587594);
		m.addAttribute("capteur", c);
		return "capteur/addForm" ;
	}
	
	@RequestMapping(value="/add" , method=RequestMethod.POST)
	public String actionAddCapteurs(Model m, @ModelAttribute("capteur") Capteur c)
	{
		if(Validators.isValid(c))
		{
		    c.setSiteCapteur(siteManager.getSiteDetails(c.getSiteCapteur()));
			capteurManager.insertCapteur(c); 
			m.addAttribute("message", Response.MSG_ADD_SUCCES);
		}
		else
			m.addAttribute("message", Response.MSG_ADD_FAILURE);
		formAddCapteurs(m);
		return "capteur/addForm" ;
	}
	
	

	@RequestMapping(value="/edit/{idCapteur}" , method=RequestMethod.GET)
	public String formEditCapteurs(Model m, @PathVariable String idCapteur)
	{
		Capteur c = new Capteur(); 
		c.setIdCapteur(idCapteur);
		c = capteurManager.getCapteurDetails(c);
		m.addAttribute("sites",siteManager.getAllSites()); 
		m.addAttribute("capteur", c); 
		return "capteur/editForm" ;
	}
	
	@RequestMapping(value="/edit/{idCapteur}" , method=RequestMethod.POST)
	public String actionEditCapteurs(Model m, @ModelAttribute("capteur") Capteur c )
	{
		if(Validators.isValid(c))
		{
			capteurManager.updateCapteur(c); 
			m.addAttribute("message", Response.MSG_ADD_SUCCES);
		}
		else
		{
			m.addAttribute("message", Response.MSG_ADD_FAILURE);
		}
		return "capteur/editForm" ;
	}
	
	@RequestMapping(value="/delete/{idCapteur}" , method=RequestMethod.GET)
	public String actionEditCapteurs(Model m, @PathVariable String idCapteur)
	{
		Capteur c = new Capteur() ; 
		c.setIdCapteur(idCapteur);
		
		if(capteurManager.getCapteurDetails(c) != null )
		{
			capteurManager.removeCapteur(c); 
			m.addAttribute("message", Response.MSG_DEL_SUCCES);
		}
		else
		{
			m.addAttribute("message", Response.MSG_DEL_FAILURE);
		}
		List<Capteur> l = capteurManager.getAllCapteurs() ; 
		m.addAttribute("capteurs", l); 
		return "capteur/list" ;
	}
	
	
}
