package com.sqli.TempSens.managers;

import java.util.List;

import com.sqli.TempSens.dao.CapteurDao;
import com.sqli.TempSens.dao.ReleveDao;
import com.sqli.TempSens.dao.SiteDao;
import com.sqli.TempSens.entities.Capteur;
import com.sqli.TempSens.entities.Releve;
import com.sqli.TempSens.entities.Site;

public class SiteManager {

	private CapteurDao capteurDao ; 
	private ReleveDao releveDao ;
	private SiteDao siteDao ;
	
	public void setSiteDao(SiteDao siteDao) {
		this.siteDao = siteDao;
	}
	public void setCapteurDao(CapteurDao capteurDao) {
		this.capteurDao = capteurDao;
	}
	public void setReleveDao(ReleveDao releveDao) {
		this.releveDao = releveDao;
	}
	
	
	public Site getSiteDetails(Site s) {
		return siteDao.getSiteDetails(s.getIdSite());
	}
	public List<Site> getAllSites() {
		return siteDao.getAllSites();
	}
	public List<Capteur> getAllCapteursInSite(Site s) {
		
		return capteurDao.getCapteursInSite(s.getIdSite());
	}
	public List<Releve> getAllRelevesInSite(Site s) {
		
		return releveDao.getRelevesInSite(s.getIdSite());
	}
	public void insertSite(Site s) {
		siteDao.insertSite(s);
		
	}
	public void deleteSite(Site s) {
		siteDao.deleteSite(s.getIdSite());
		
	} 
	
	
	
	
}
