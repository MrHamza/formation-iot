package com.sqli.TempSens.controllers;

import java.lang.reflect.Method;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sqli.TempSens.entities.Site;
import com.sqli.TempSens.entities.Validators;
import com.sqli.TempSens.managers.SiteManager;
import com.sqli.TempSens.response.Response;

@Controller
@RequestMapping("/site")
public class SitesController {

	@Autowired
	private SiteManager siteManager ; 
	
	@RequestMapping(value="/list")
	public String listSites(Model m){
		List<Site> l = siteManager.getAllSites() ; 
		m.addAttribute("sites", l); 
		return "site/list"; 
	}
	
	@RequestMapping(value="/add" , method=RequestMethod.GET)
	public String formAddSite(Model m )
	{
		m.addAttribute("site", new Site()); 
		return "site/addForm";
	}
	
	@RequestMapping(value="/add" , method=RequestMethod.POST)
	public String actionAddSite(Model m , @ModelAttribute("site") Site s)
	{
		if(Validators.isValid(s))
		{
			siteManager.insertSite(s); 
			m.addAttribute("message", Response.MSG_ADD_SUCCES);
		}
		else
		{
			m.addAttribute("message", Response.MSG_ADD_FAILURE);
		}
		
		return  formAddSite( m );
	}
	
	@RequestMapping(value="/delete/{idSite}" , method=RequestMethod.GET)
	public String actionDelSite(Model m , @PathVariable String idSite)
	{
		Site s = new Site() ; 
		s.setIdSite(idSite);
		
		if(siteManager.getSiteDetails(s) != null )
		{
			siteManager.deleteSite(s); 
			m.addAttribute("message", Response.MSG_DEL_SUCCES);
		}
		else 
		{
			
			m.addAttribute("message", Response.MSG_DEL_FAILURE);
		}
		List<Site> l = siteManager.getAllSites() ; 
		m.addAttribute("sites", l); 
		return "site/list";
	}
	
}
