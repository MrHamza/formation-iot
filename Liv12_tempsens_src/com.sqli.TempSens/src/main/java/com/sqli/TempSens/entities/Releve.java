package com.sqli.TempSens.entities;

import java.util.Date;



import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "releves")
public class Releve {
	
	@Id
	private String id ; 
	private float temperature ; 
	private String idCapteur ; 
	private String libelleCapteur ; 
	private double latCapteur ; 
	private double lngCapteur ; 
	private float seuilMax ; 
	private float seuilMin ; 
	private String idSite ; 
	private String libelleSite ; 
	private double latSite ; 
	private double lngSite ; 
	private Date dateReleve ;
	
	/* CONSTRUCTEUR */ 
	public Releve() {

	}
	
	/* GETTERS ET SETTERS */
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public float getTemperature() {
		return temperature;
	}
	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}
	public String getIdCapteur() {
		return idCapteur;
	}
	public void setIdCapteur(String idCapteur) {
		this.idCapteur = idCapteur;
	}
	public String getLibelleCapteur() {
		return libelleCapteur;
	}
	public void setLibelleCapteur(String libelleCapteur) {
		this.libelleCapteur = libelleCapteur;
	}
	public double getLatCapteur() {
		return latCapteur;
	}
	public void setLatCapteur(double latCapteur) {
		this.latCapteur = latCapteur;
	}
	public double getLngCapteur() {
		return lngCapteur;
	}
	public void setLngCapteur(double lngCapteur) {
		this.lngCapteur = lngCapteur;
	}
	public float getSeuilMax() {
		return seuilMax;
	}
	public void setSeuilMax(float seuilMax) {
		this.seuilMax = seuilMax;
	}
	public float getSeuilMin() {
		return seuilMin;
	}
	public void setSeuilMin(float seuilMin) {
		this.seuilMin = seuilMin;
	}
	public String getIdSite() {
		return idSite;
	}
	public void setIdSite(String idSite) {
		this.idSite = idSite;
	}
	public String getLibelleSite() {
		return libelleSite;
	}
	public void setLibelleSite(String libelleSite) {
		this.libelleSite = libelleSite;
	}
	public double getLatSite() {
		return latSite;
	}
	public void setLatSite(double latSite) {
		this.latSite = latSite;
	}
	public double getLngSite() {
		return lngSite;
	}
	public void setLngSite(double lngSite) {
		this.lngSite = lngSite;
	}
	public Date getDateReleve() {
		return dateReleve;
	}
	public void setDateReleve(Date dateReleve) {
		this.dateReleve = dateReleve;
	}
	
	/* TO STRING */ 
	@Override
	public String toString() {
		return "Releve [id=" + id + ", temperature=" + temperature
				+ ", idCapteur=" + idCapteur + ", libelleCapteur="
				+ libelleCapteur + ", latCapteur=" + latCapteur
				+ ", lngCapteur=" + lngCapteur + ", seuilMax=" + seuilMax
				+ ", seuilMin=" + seuilMin + ", idSite=" + idSite
				+ ", libelleSite=" + libelleSite + ", latSite=" + latSite
				+ ", lngSite=" + lngSite + ", dateReleve=" + dateReleve + "]";
	}
	
	
}
