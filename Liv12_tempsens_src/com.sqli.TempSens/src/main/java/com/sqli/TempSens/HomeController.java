package com.sqli.TempSens;

import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	/**
	 * Monitor
	 */
	@RequestMapping(value = {"/Monitor","/Monitor/","/",""}, method = RequestMethod.GET)
	public String monitor(Locale locale, Model model) {
		
		return "monitor/index";
	}
	
	/**
	 * formulaire d'authentification
	 */
	
	@RequestMapping(value = {"/login","/login/"}, method = RequestMethod.GET)
	public ModelAndView login(
		@RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout) {
 
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Mot de passe ou/et bien login incorrecte(s)");
		}
 
		if (logout != null) {
			model.addObject("message", "Vous �tes deconnect� avec succes");
		}
		model.setViewName("home");
 
		return model;
 
	}
	
}
