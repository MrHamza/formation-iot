package com.sqli.TempSens.entities;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="sites")
public class Site {
	
	@Id
	private String idSite ; 
	private String libelleSite ; 
	private double latSite ; 
	private double lngSite ;
	
	
	public String getIdSite() {
		return idSite;
	}
	public void setIdSite(String idSite) {
		this.idSite = idSite;
	}
	public String getLibelleSite() {
		return libelleSite;
	}
	public void setLibelleSite(String libelleSite) {
		this.libelleSite = libelleSite;
	}
	public double getLatSite() {
		return latSite;
	}
	public void setLatSite(double latSite) {
		this.latSite = latSite;
	}
	public double getLngSite() {
		return lngSite;
	}
	public void setLngSite(double lngSite) {
		this.lngSite = lngSite;
	}
	@Override
	public String toString() {
		return "Site [idSite=" + idSite + ", libelleSite=" + libelleSite
				+ ", latSite=" + latSite + ", lngSite=" + lngSite + "]";
	}
	
	public Site(String idSite, String libelleSite, double latSite,
			double lngSite) {
		super();
		this.idSite = idSite;
		this.libelleSite = libelleSite;
		this.latSite = latSite;
		this.lngSite = lngSite;
	}
	public Site() {
		
	} 
	
	
}
