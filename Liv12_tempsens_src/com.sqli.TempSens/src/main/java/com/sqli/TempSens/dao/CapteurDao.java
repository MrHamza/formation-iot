package com.sqli.TempSens.dao;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.sqli.TempSens.config.SpringMongoConfig;
import com.sqli.TempSens.entities.Capteur;
import com.sqli.TempSens.entities.Releve;

public class CapteurDao {
private MongoOperations mongoOperation; 
	
	
	public CapteurDao(){
		ApplicationContext ctx = 
	             new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		mongoOperation = (MongoOperations)ctx.getBean("mongoTemplate");
	}
	
	/**
	 * Recuperer la liste de tous les capteurs 
	 * @return
	 */
	public List<Capteur> getAllCapteurs(){
		return mongoOperation.findAll(Capteur.class); 
	}
	
	/**
	 * Retourner des informations a propos d'un capteur dont l'ID est pass� en paramete
	 * @param idCapteur
	 * @return
	 */
	public Capteur getCapteurDetails(String idCapteur)
	{
		Query searchQuery = new Query(Criteria.where("idCapteur").is(idCapteur));
		return mongoOperation.findOne(searchQuery, Capteur.class); 
	}
	
	/**
	 * Recuperer la liste des capteurs dans un site pass� en param 
	 * @param idSite
	 * @return
	 */
	public List<Capteur> getCapteursInSite(String idSite)
	{
		Query searchQuery = new Query(Criteria.where("idSite").is(idSite));
		return mongoOperation.find(searchQuery, Capteur.class);
	}
	
	/**
	 * Supprimer un Capteur a partir de son ID 
	 * @param idCapteur
	 */
	public void deleteCapteur(String idCapteur){
		Query searchQuery = new Query(Criteria.where("idCapteur").is(idCapteur));
	    mongoOperation.remove(searchQuery, Capteur.class);
	}

	public void insertCapteur(Capteur c) {
		mongoOperation.save(c);
		
	}
	
	

}
