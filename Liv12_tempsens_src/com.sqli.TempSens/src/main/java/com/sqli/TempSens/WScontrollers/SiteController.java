package com.sqli.TempSens.WScontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sqli.TempSens.entities.Site;
import com.sqli.TempSens.managers.ReleveManager;
import com.sqli.TempSens.managers.SiteManager;
import com.sqli.TempSens.response.Response;

@RestController
@RequestMapping(value="/sites")
public class SiteController {

	@Autowired
	ReleveManager releveManager ; 
	
	@Autowired
	SiteManager siteManager ; 
	
	@RequestMapping(value={"/{idSite}/","/{idSite}"}, method=RequestMethod.GET)
	public Object getSiteDetails(@PathVariable String idSite)
	{
		Response resu = new Response() ; 
		
		Site s = new Site(); 
		s.setIdSite(idSite);
		s = siteManager.getSiteDetails(s);
		if(s == null )
		{
			resu.setObj(null);
			resu.setStatus(Response.FAILURE);
			resu.setMsg(Response.MSG_GET_FAILURE);
		}
		else
		{
			resu.setObj(s);
			resu.setStatus(Response.SUCCES);
			resu.setMsg(Response.MSG_GET_SUCCES);
		}
		return resu ; 
	}
	
	
	@RequestMapping(value={"/",""}, method=RequestMethod.GET)
	public Object getAllSites()
	{
		Response resu = new Response() ; 
		
	
		resu.setObj(siteManager.getAllSites());
		resu.setStatus(Response.SUCCES);
		resu.setMsg(Response.MSG_GET_SUCCES);
		
		return resu ; 
	}
	
	@RequestMapping(value={"/{idSite}/capteurs","/{idSite}/capteurs/"}, method=RequestMethod.GET)
	public Object getCapteursInSite(@PathVariable String idSite)
	{
		Response resu = new Response() ; 
		
		Site s = new Site(); 
		s.setIdSite(idSite);
		s = siteManager.getSiteDetails(s);
		
		if(s == null )
		{
			resu.setObj(null);
			resu.setStatus(Response.FAILURE);
			resu.setMsg(Response.MSG_GET_FAILURE);
		}
		else
		{
			resu.setObj(siteManager.getAllCapteursInSite(s));
			resu.setStatus(Response.SUCCES);
			resu.setMsg(Response.MSG_GET_SUCCES);
		}
		return resu ; 
	}
	
	@RequestMapping(value={"/{idSite}/releves","/{idSite}/releves/"}, method=RequestMethod.GET)
	public Object getRelevesInSite(@PathVariable String idSite)
	{
		Response resu = new Response() ; 
		
		Site s = new Site(); 
		s.setIdSite(idSite);
		s = siteManager.getSiteDetails(s);
		
		if(s == null )
		{
			resu.setObj(null);
			resu.setStatus(Response.FAILURE);
			resu.setMsg(Response.MSG_GET_FAILURE);
		}
		else
		{
			resu.setObj(siteManager.getAllRelevesInSite(s));
			resu.setStatus(Response.SUCCES);
			resu.setMsg(Response.MSG_GET_SUCCES);
		}
		return resu ; 
	}
	
}
