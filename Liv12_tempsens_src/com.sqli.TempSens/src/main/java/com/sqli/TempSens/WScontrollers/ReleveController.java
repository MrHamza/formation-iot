package com.sqli.TempSens.WScontrollers;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sqli.TempSens.entities.Releve;
import com.sqli.TempSens.managers.ReleveManager;
import com.sqli.TempSens.response.Response;

/**
 * Controlleur des relev�s de la chaleur
 */
@RestController
@RequestMapping(value = "/releves")
public class ReleveController {
	
	@Autowired
	ReleveManager releveManager; 
	
	
	/**
	 * Recuperer la liste de tous les relev�s 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"/",""}, method = RequestMethod.GET)
	public Object getAllReleves( Model model) {
		Response resu = new Response() ; 
		resu.setObj(releveManager.getAllReleves());
		resu.setStatus(Response.SUCCES);
		resu.setMsg(Response.MSG_GET_SUCCES);
		
		return resu; 
	}
	
	/**
	 * Recuperer les details a propos d'un relev�  
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{idReleve}", method = RequestMethod.GET)
	public Object getReleve( Model model, @PathVariable String idReleve) {
		
		Response resu = new Response() ; 
		Releve r = new Releve() ; 
		r.setId(idReleve);
		r=releveManager.getReleveDetails(r); 
		if(r != null )
		{
			resu.setObj(r);
			resu.setStatus(Response.SUCCES);
			resu.setMsg(Response.MSG_GET_SUCCES);
		}
		else
		{
			resu.setObj(r);
			resu.setStatus(Response.FAILURE);
			resu.setMsg(Response.MSG_GET_FAILURE);
		}
		
		return resu; 
		
	}
	
	/**
	 * Supprimer un relev�   
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{idReleve}", method = RequestMethod.DELETE)
	public Object insertReleve( Model model, @PathVariable String idReleve) {
		Releve r = new Releve() ; 
		r.setId(idReleve);
		Response resu = new Response() ; 
		if(releveManager.getReleveDetails(r) != null )
		{
			releveManager.deleteReleve(r);
			resu.setObj(null);
			resu.setStatus(Response.SUCCES);
			resu.setMsg(Response.MSG_DEL_SUCCES);
		}
		else
		{
			resu.setObj(null);
			resu.setStatus(Response.FAILURE);
			resu.setMsg(Response.MSG_DEL_FAILURE);
		}
		
		
		return resu; 
	}
	
	
	
	
}