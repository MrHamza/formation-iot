package com.sqli.TempSens.managers;

import java.util.List;

import com.sqli.TempSens.dao.ReleveDao;
import com.sqli.TempSens.entities.Capteur;
import com.sqli.TempSens.entities.Releve;
import com.sqli.TempSens.entities.Site;

public class ReleveManager {
	
		private ReleveDao releveDao ;

		public void setReleveDao(ReleveDao releveDao) {
			this.releveDao = releveDao;
		} 
		
		public List<Releve> getAllReleves(){
			
			return releveDao.getAllReleves()  ; 
		}
		
		public List<Releve> getRelevesInSite(Site s){
			
			return releveDao.getRelevesInSite(s.getIdSite()) ; 
		}
		
		public List<Releve> getRelevesInCapteur(Capteur c){
			return releveDao.getRelevesInCapteur(c.getIdCapteur()) ; 
		}
		
		public Releve getReleveDetails(Releve r)
		{
			return releveDao.getReleveById(r.getId()); 
		}
		
		public void deleteReleve(Releve r)
		{
			 releveDao.deleteReleve(r.getId()); 
		}

		public void insertReleve(Releve r) {
			releveDao.insertReleve(r);
			
		}
		
		
		
	
}
