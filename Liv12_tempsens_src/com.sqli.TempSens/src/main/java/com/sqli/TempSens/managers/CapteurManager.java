package com.sqli.TempSens.managers;

import java.util.List;

import com.sqli.TempSens.dao.CapteurDao;
import com.sqli.TempSens.dao.ReleveDao;
import com.sqli.TempSens.entities.Capteur;
import com.sqli.TempSens.entities.Releve;

public class CapteurManager {

	private ReleveDao releveDao ;
	private CapteurDao capteurDao ; 
	
	public void setReleveDao(ReleveDao releveDao) {
		this.releveDao = releveDao;
	} 
	
	public void setCapteurDao(CapteurDao capteurDao) {
		this.capteurDao = capteurDao;
	} 
	
	public List<Releve> getAllReleveInCapteur(Capteur c)
	{
		return releveDao.getRelevesInCapteur(c.getIdCapteur()); 
		
		
	}

	public Capteur getCapteurDetails(Capteur c) {
		return capteurDao.getCapteurDetails(c.getIdCapteur()); 
		
	}

	public List<Capteur> getAllCapteurs() {
		
		return capteurDao.getAllCapteurs();
	}

	public void insertCapteur(Capteur c) {
		capteurDao.insertCapteur(c); 
		
	}

	public void updateCapteur(Capteur c) {
		capteurDao.insertCapteur(c);
		
	}

	public void removeCapteur(Capteur c) {
		capteurDao.deleteCapteur(c.getIdCapteur());
		
	}

	public List<Releve> getAllReleveInCapteurLimited(Capteur c, int limited) {
		return releveDao.getAllReleveInCapteurLimited(c.getIdCapteur(), limited);
	}
	
	
	
}
