package com.sqli.TempSens.WScontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sqli.TempSens.entities.Capteur;
import com.sqli.TempSens.entities.Releve;
import com.sqli.TempSens.managers.CapteurManager;
import com.sqli.TempSens.response.Response;

@RestController
@RequestMapping(value="/capteurs")
public class CapteurController {
	
	@Autowired
	CapteurManager capteurManager ; 
	/**
	 * Recuperer la liste de tous les capteurs 
	 * @return
	 */
	@RequestMapping(value = {"/",""} , method = RequestMethod.GET)
	public Object getAllCapteurs()
	{
		Response resu = new Response() ; 
		
		resu.setMsg(Response.MSG_GET_SUCCES);
		resu.setStatus(Response.SUCCES);
		resu.setObj(capteurManager.getAllCapteurs());
		
		return resu ; 
	}
	
	/**
	 * Recuperer les details a propos d'un capteur dont l'ID est pass� en params 
	 * @return
	 */
	@RequestMapping(value = {"/{idCapteur}"} , method = RequestMethod.GET)
	public Object getCapteurDetails(@PathVariable String idCapteur)
	{
		Response resu = new Response() ; 
		Capteur c = new Capteur() ; 
		c.setIdCapteur(idCapteur);
		c=  capteurManager.getCapteurDetails(c); 
		
		if(c == null )
		{
			resu.setMsg(Response.MSG_GET_FAILURE);
			resu.setStatus(Response.FAILURE);
			resu.setObj(null);
		}
		else
		{
			resu.setMsg(Response.MSG_GET_SUCCES);
			resu.setStatus(Response.SUCCES);
			resu.setObj(c);
		}
		return resu ; 
	}
	
	
	
	/**
	 * Recuperer la liste des releves faites par un capteur  
	 * @return
	 */
	@RequestMapping(value = {"/{idCapteur}/releves","/{idCapteur}/releves/"} , method = RequestMethod.GET)
	public Object getRelevesInCapteur(@PathVariable String idCapteur )
	{
		Capteur c = new Capteur() ; 
		c.setIdCapteur(idCapteur);
		List<Releve> l = capteurManager.getAllReleveInCapteur(c); 
		Response resu = new Response() ; 
		
		if(l == null )
		{
			resu.setMsg(Response.MSG_GET_FAILURE);
			resu.setStatus(Response.FAILURE);
			resu.setObj(null);
		}
		else
		{
			resu.setMsg(Response.MSG_GET_SUCCES);
			resu.setStatus(Response.SUCCES);
			resu.setObj(l);
		}
		
		return resu ;
	}
	
	
	/**
	 * Recuperer la liste des releves faites par un capteur  avec une limite
	 * @return
	 */
	@RequestMapping(value = {"/{idCapteur}/releves/limit/{limit}","/{idCapteur}/releves/limit/{limit}/"} , method = RequestMethod.GET)
	public Object getRelevesInCapteurLimited(@PathVariable String idCapteur , @PathVariable String limit )
	{
		Capteur c = new Capteur() ; 
		c.setIdCapteur(idCapteur);
		List<Releve> l = null ; 
		Response resu = new Response() ; 
		int limited = 10 ; 
		
		try{
			limited = Integer.parseInt(limit); 
			l =capteurManager.getAllReleveInCapteurLimited(c,limited) ;
			if(l != null )
			{
				resu.setObj(l);
				resu.setMsg(Response.MSG_GET_SUCCES);
				resu.setStatus(Response.SUCCES);
			}
			else
			{
				resu.setMsg(Response.MSG_GET_FAILURE);
				resu.setStatus(Response.FAILURE);
				resu.setObj(null);
			}
			
		}
		catch(Exception ex)
		{
			resu.setMsg(Response.MSG_GET_FAILURE);
			resu.setStatus(Response.FAILURE);
			resu.setObj(null);
		}
		
		
		return resu ;
	}
	
	
	/**
	 * Suppeimer un capteur dont l'ID est pass� en parametre  
	 * @return
	 */
	@RequestMapping(value = {"/{idCapteur}/","/{idCapteur}"} , method = RequestMethod.DELETE)
	public Object deleteCapteur(@PathVariable String idCapteur )
	{
		Capteur c = new Capteur() ; 
		c.setIdCapteur(idCapteur);
		Response resu = new Response() ; 
		
		if(capteurManager.getCapteurDetails(c) == null )
		{
			resu.setMsg(Response.MSG_DEL_FAILURE);
			resu.setStatus(Response.FAILURE);
			resu.setObj(null);
		}
		else
		{
			resu.setMsg(Response.MSG_DEL_SUCCES);
			resu.setStatus(Response.SUCCES);
			resu.setObj(null);
		}
		
		return resu ;
	}
	
	
}
