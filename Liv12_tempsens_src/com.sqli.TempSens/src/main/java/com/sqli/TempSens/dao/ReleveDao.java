package com.sqli.TempSens.dao;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.sqli.TempSens.config.SpringMongoConfig;
import com.sqli.TempSens.entities.Releve;


public class ReleveDao {
	
	
	private MongoOperations mongoOperation; 
	
	
	public ReleveDao(){
		ApplicationContext ctx = 
	             new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		mongoOperation = (MongoOperations)ctx.getBean("mongoTemplate");
	}
	
	/**
	 * Inserer un nouveau relev� 
	 * @param r
	 */
	public void insertReleve(Releve r)
	{
		mongoOperation.insert(r);
	}
	
	
	/**
	 * Recuperer les details a propos d'un relev� a partir de son Id 
	 * @param idReleve
	 * @return
	 */
	public Releve getReleveById(String idReleve)
	{
		Query searchQuery = new Query(Criteria.where("id").is(idReleve));
		return mongoOperation.findOne(searchQuery, Releve.class);
	}
	
	/**
	 * Recuperer la liste de tous les relev�s 
	 * @return
	 */
	public List<Releve> getAllReleves()
	{
		return mongoOperation.findAll(Releve.class); 
	}
	
	/**
	 * Recuperer la list des relev� dans un site pass� en parametre 
	 * @param idSite
	 * @return
	 */
	public List<Releve> getRelevesInSite(String idSite)
	{
		Query searchQuery = new Query(Criteria.where("idSite").is(idSite));
		return mongoOperation.find(searchQuery, Releve.class);
	}
	
	/**
	 * Recuperer la liste des relev� concernant un capteur pass� en parametre 
	 * @param idCapteur
	 * @return
	 */
	public List<Releve> getRelevesInCapteur(String idCapteur)
	{
		Query searchQuery = new Query(Criteria.where("idCapteur").is(idCapteur));
		return mongoOperation.find(searchQuery, Releve.class);
	}
	
	/**
	 * Supprimer un relev� par son ID 
	 * @param idReleve
	 */
	public void deleteReleve(String idReleve)
	{
		Query searchQuery = new Query(Criteria.where("idReleve").is(idReleve));
	    mongoOperation.remove(searchQuery, Releve.class);
	}
	
	/**
	 * Supprimer tous les relev� dans un site 
	 * @param idSite
	 */
	public void deleteAllRelevesInSite(String idSite)
	{
		Query searchQuery = new Query(Criteria.where("idSite").is(idSite));
	    mongoOperation.remove(searchQuery, Releve.class);
	}
	
	/**
	 * Supprimer tous les relev�s d'un capteur
	 * @param idCapteur
	 */
	public void deleteAllRelevesInCapteur(String idCapteur)
	{
		Query searchQuery = new Query(Criteria.where("idCapteur").is(idCapteur));
	    mongoOperation.remove(searchQuery, Releve.class);
	}

	public List<Releve> getAllReleveInCapteurLimited(String idCapteur,
			int limited) {
		
		//BasicQuery searchQuery = new BasicQuery("{idCapteur :'"+idCapteur+"' , $sort :{dateReleve: 1}, $limit:"+limited+"}");
		Query searchQuery = new Query(Criteria.where("idCapteur").is(idCapteur));
		searchQuery.limit(limited);
		searchQuery.with(new Sort(Sort.Direction.DESC,"dateReleve"));
		return mongoOperation.find(searchQuery, Releve.class);
		
	}
	
	
}
