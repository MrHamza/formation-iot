package com.sqli.TempSens.dao;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.sqli.TempSens.config.SpringMongoConfig;
import com.sqli.TempSens.entities.Site;

public class SiteDao {

private MongoOperations mongoOperation; 
	
	
	public SiteDao(){
		ApplicationContext ctx = 
	             new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		mongoOperation = (MongoOperations)ctx.getBean("mongoTemplate");
	}
	
	public Site getSiteDetails(String idSite)
	{
		Query searchQuery = new Query(Criteria.where("idSite").is(idSite));
		return mongoOperation.findOne(searchQuery, Site.class); 
	}
	
	public List<Site> getAllSites()
	{
		
		return mongoOperation.findAll(Site.class); 
	}
	
	public void deleteSite(String idSite)
	{
		Query searchQuery = new Query(Criteria.where("idSite").is(idSite));
		mongoOperation.remove(searchQuery, Site.class); 
	}
	
	public void insertSite(Site s)
	{
		mongoOperation.insert(s);
	}
}
