package com.sqli.TempSens.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "capteurs")
public class Capteur {

	@Id
	private String idCapteur; 
	private String label ; 
	private double latCapteur ; 
	private double lngCapteur; 
	private Site siteCapteur ; 
	private float seuilMax ; 
	private float seuilMin ;
	
	
	public String getIdCapteur() {
		return idCapteur;
	}
	public void setIdCapteur(String idCapteur) {
		this.idCapteur = idCapteur;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public double getLatCapteur() {
		return latCapteur;
	}
	public void setLatCapteur(double latCapteur) {
		this.latCapteur = latCapteur;
	}
	public double getLngCapteur() {
		return lngCapteur;
	}
	public void setLngCapteur(double lngCapteur) {
		this.lngCapteur = lngCapteur;
	}
	public Site getSiteCapteur() {
		return siteCapteur;
	}
	public void setSiteCapteur(Site siteCapteur) {
		this.siteCapteur = siteCapteur;
	}
	public float getSeuilMax() {
		return seuilMax;
	}
	public void setSeuilMax(float seuilMax) {
		this.seuilMax = seuilMax;
	}
	public float getSeuilMin() {
		return seuilMin;
	}
	public void setSeuilMin(float seuilMin) {
		this.seuilMin = seuilMin;
	}
	@Override
	public String toString() {
		return "Capteur [idCapteur=" + idCapteur + ", label=" + label
				+ ", latCapteur=" + latCapteur + ", lngCapteur=" + lngCapteur
				+ ", siteCapteur=" + siteCapteur + ", seuilMax=" + seuilMax
				+ ", seuilMin=" + seuilMin + "]";
	}
	
	public Capteur() {
		
	}
	public Capteur(String idCapteur, String label, double latCapteur,
			double lngCapteur, Site siteCapteur, float seuilMax, float seuilMin) {
		super();
		this.idCapteur = idCapteur;
		this.label = label;
		this.latCapteur = latCapteur;
		this.lngCapteur = lngCapteur;
		this.siteCapteur = siteCapteur;
		this.seuilMax = seuilMax;
		this.seuilMin = seuilMin;
	} 

	
}
