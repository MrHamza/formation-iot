package com.sqli.TempSens.Listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class MyContextListener implements ServletContextListener {

	
	@Override
     public void contextInitialized(ServletContextEvent servletContextEvent) {
            WebApplicationContextUtils.getRequiredWebApplicationContext(servletContextEvent.getServletContext())
           .getAutowireCapableBeanFactory().autowireBean(this);
            //ApplicationContext context = new ClassPathXmlApplicationContext("MqttContext.xml");
            System.out.println("ContexInitialized");
            
     }

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
}