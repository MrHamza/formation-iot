<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ajout d'un nouveau site</title>

<link
	href="<%=getServletContext().getContextPath()%>/resources/css/style.css"
	rel="stylesheet">
<link
	href="<%=getServletContext().getContextPath()%>/resources/css/style-responsive.css"
	rel="stylesheet">
<script type="text/javascript"
	src="<%=getServletContext().getContextPath()%>/resources/js/jquery-2.1.3.min.js"></script>
<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=true"></script>
<style type="text/css">
.scroll {
	width: 100%;
	height: 200px;
	overflow-y: scroll;
}

.chartContainer {
	height: 300px;
	width: 100%;
}

.main-content {
	margin-left: 0px;
}

.sticky-header .header-section {
	left: 0px;
}

.panel-body {
	padding: 20px;
}
</style>
</head>

<body class="sticky-header">
	<section>
		<!-- main content start-->
		<div class="main-content">

			<jsp:include page="../header.jsp"></jsp:include>


			<!--body wrapper start-->
			<div class="wrapper">

				<div class="row">
					<div class="col-xs-12 col-md-8">
						<section class="panel">
							<header class="panel-heading"> Ajout d'un site </header>
							<div class="panel-body">
								<c:if test="${!empty message }">
									<div class="alert alert-warning alert-dismissible" role="alert">
										<button type="button" class="close" data-dismiss="alert"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										${message}
									</div>
								</c:if>
								<f:form class="form-horizontal" modelAttribute="site">
									<div class="form-group">
										<!-- IDENTIFIANT -->
										<label for="desc1">Identifiant du site</label>
										<f:input class="form-control" path="idSite" placeholder="ID"
											id="desc1" />
									</div>
									<div class="form-group">
										<div style="color:#d9534f">Remarque : Vous pouvez modifier les informations d'un site en entrant son ID dans le champs "Identifiant du site", ainsi toute information entr�e va �craser celle du site en question</div>
									</div>
									<div class="form-group">
										<!-- LIBELLE SITE -->
										<label for="desc2">Libell� du site</label>
										<f:input class="form-control" path="libelleSite"
											placeholder="LIBELLE" id="desc2" />
									</div>
									
									<f:hidden path="latSite" id="lat" />
									<f:hidden path="lngSite" id="lng" />
									<div class="form-group">
										<div style="color:#d9534f">Remarque : Veuillez deplacer le curseur pour definir la localisation g�ographique du site</div>
									</div>
									<div class="form-group">
										<div id="adr" style="text-align: center;"></div>
										<div id="Gmap"
											style="width: 100%; height: 350px; border: 1px solid black"></div>
									</div>
									<div class="form-group">
										<input type="submit" class="btn btn-default"
											value="Enregistrer" />
									</div>
								</f:form>
							</div>
						</section>
					</div>

					<div class="col-xs-6 col-md-4">
						<jsp:include page="../menu.jsp"></jsp:include>
					</div>
				</div>



			</div>
			<!--body wrapper end-->

			<!--footer section start-->
			<footer> 2015 &copy; SQLI Group </footer>
			<!--footer section end-->


		</div>
		<!-- main content end-->
	</section>

</body>


<script type="text/javascript"
	src="<%=getServletContext().getContextPath()%>/resources/js/carteLoader.js"></script>
<SCRIPT TYPE="text/javascript"
	src=" <%=getServletContext().getContextPath()%>/resources/js/jquery-2.1.3.min.js"></SCRIPT>
<script
	src="<%=getServletContext().getContextPath()%>/resources/js/bootstrap.min.js"></script>

</html>