<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link
	href="<%=getServletContext().getContextPath()%>/resources/css/style.css"
	rel="stylesheet">
<link
	href="<%=getServletContext().getContextPath()%>/resources/css/style-responsive.css"
	rel="stylesheet">
<title>List des sites</title>
<style type="text/css">
.scroll {
	width: 100%;
	height: 200px;
	overflow-y: scroll;
}

.chartContainer {
	height: 300px;
	width: 100%;
}

.main-content {
	margin-left: 0px;
}

.sticky-header .header-section {
	left: 0px;
}
</style>


</head>

<body class="sticky-header">

	<section>
		<!-- main content start-->
		<div class="main-content">
			<jsp:include page="../header.jsp"></jsp:include>

	
			<div class="wrapper">


				<div class="row">
					<div class="col-xs-12 col-md-8">
						<section class="panel">
							<header class="panel-heading"> Liste des sites </header>
							<div class="panel-body">
								<c:if test="${!empty message }">
									<div class="alert alert-warning alert-dismissible" role="alert">
										<button type="button" class="close" data-dismiss="alert"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										${message}
									</div>
								</c:if>
								<a class="btn btn-primary"
									href="<%=getServletContext().getContextPath()%>/site/add">Ajouter
									un autre site</a>
								<table class="table  table-hover general-table">
									<thead>
										<tr>
											<th>#</th>
											<th>Libelle</th>
											<th>Latitude</th>
											<th>Longitude</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="site" items="${sites}">
											<tr>
												<td>${site.idSite}</td>
												<td>${site.libelleSite}</td>
												<td>${site.latSite}</td>
												<td>${site.lngSite}</td>
												<td><a class="btn btn-warning"
													href="<%=getServletContext().getContextPath()%>/site/delete/${site.idSite}">Supprimer</a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>



								<SCRIPT TYPE="text/javascript"
									src=" <%=getServletContext().getContextPath()%>/resources/js/jquery-2.1.3.min.js"></SCRIPT>
								<script
									src="<%=getServletContext().getContextPath()%>/resources/js/bootstrap.min.js"></script>
							</div>
						</section>
					</div>
					<div class="col-xs-6 col-md-4">
						<jsp:include page="../menu.jsp"></jsp:include>
					</div>
				</div>



			</div>
			<!--body wrapper end-->

			<!--footer section start-->
			<footer> 2015 &copy; SQLI Group </footer>
			<!--footer section end-->


		</div>
		<!-- main content end-->
	</section>


</body>





</html>
