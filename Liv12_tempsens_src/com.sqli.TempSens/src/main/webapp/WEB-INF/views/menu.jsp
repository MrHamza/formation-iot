<div class="list-group">
  <a href="<%=getServletContext().getContextPath()%>/Monitor" class="list-group-item">
    <h4 class="list-group-item-heading">Monitoring</h4>
    <p class="list-group-item-text">Suivi en temps r�el de l'�volution de la temp�rature au niveau des diff�rents capteurs </p>
  </a>
  
  <a href="<%=getServletContext().getContextPath()%>/capteur/list" class="list-group-item">
    <h4 class="list-group-item-heading">Liste des capteurs</h4>
    <p class="list-group-item-text">Liste de tous les capteurs disponibles</p>
  </a>
  
  <a href="<%=getServletContext().getContextPath()%>/capteur/add" class="list-group-item">
    <h4 class="list-group-item-heading">Ajout d'un capteur</h4>
    <p class="list-group-item-text">Assistant pour l'ajout et la configuration d'un nouveau capteur</p>
  </a>
  
  <a href="<%=getServletContext().getContextPath()%>/site/list" class="list-group-item">
    <h4 class="list-group-item-heading">Liste des sites</h4>
    <p class="list-group-item-text">Liste de tous les sites disponibles</p>
  </a>
  
  <a href="<%=getServletContext().getContextPath()%>/site/add" class="list-group-item">
    <h4 class="list-group-item-heading">Ajout d'un site</h4>
    <p class="list-group-item-text">Assistant pour l'ajout et la configuration d'un nouveau site</p>
  </a>
</div>