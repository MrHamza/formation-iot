<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

<title>Temperature Monitor</title>

<link
	href="<%=getServletContext().getContextPath()%>/resources/css/style.css"
	rel="stylesheet">
<link
	href="<%=getServletContext().getContextPath()%>/resources/css/style-responsive.css"
	rel="stylesheet">


<style type="text/css">
.scroll {
	width: 100%;
	height: 200px;
	overflow-y: scroll;
}

.chartContainer {
	height: 300px;
	width: 100%;
}

.main-content {
	margin-left: 0px;
}

.sticky-header .header-section {
	left: 0px;
}
</style>
</head>

<body class="sticky-header">

	<section>

		<!-- main content start-->
		<div class="main-content">
			<jsp:include page="../header.jsp"></jsp:include>


			
			<!--body wrapper start-->
			<div class="wrapper">
				<button class="btn btn-success" onclick="client.connect(options);">D�marrer
					le suivi</button>
				<button class="btn btn-warning" onclick="client.disconnect();">Arr�ter
					le suivi</button>

				<div class="row">
					<div class="col-xs-12 col-md-8">
						<section class="panel">
							<header class="panel-heading"> SUIVI EN TEMPS REEL DE
								L'EVOLUTION DES TEMPERATURES </header>
							<div class="panel-body"  id="body">
								<div class="alert alert-warning"  role="alert"><strong id="msgAlert">Aucune information re�ue</strong></div>
								<!-- ICI L'AJOUT DES GRAPHS EN JAVASCRIPT utils.js -->
							</div>
						</section>
					</div>
					<div class="col-xs-6 col-md-4">
						<jsp:include page="../menu.jsp"></jsp:include>
					</div>
				</div>



				<div class="row">
					<div class="col-sm-12">
						<section class="panel">
							<header class="panel-heading"> Logger </header>
							<div class="panel-body">
								<table class="table  table-hover general-table">
									<thead>
										<tr>
											<th>Date</th>
											<th>Adresse du site</th>
											<th>Temperature</th>

										</tr>
									</thead>
									<tbody class="scroll" id="messages">

									</tbody>
								</table>
							</div>
						</section>
					</div>
				</div>



			</div>
			<!--body wrapper end-->

			<!--footer section start-->
			<footer> 2015 &copy; SQLI Group </footer>
			<!--footer section end-->


		</div>
		<!-- main content end-->
	</section>

	<script
		src="<%=getServletContext().getContextPath()%>/resources/js/jquery-migrate-1.2.1.min.js"></script>
	<script
		src="<%=getServletContext().getContextPath()%>/resources/js/bootstrap.min.js"></script>




	<SCRIPT TYPE="text/javascript"
		src="http://www.hivemq.com/demos/websocket-client/js/mqttws31.js"></SCRIPT>
		
		<SCRIPT TYPE="text/javascript"
		src="<%=getServletContext().getContextPath()%>/resources/js/date-formater.js"></SCRIPT>
	<SCRIPT TYPE="text/javascript"
		src=" <%=getServletContext().getContextPath()%>/resources/js/jquery-2.1.3.min.js"></SCRIPT>
	<SCRIPT TYPE="text/javascript"
		src="<%=getServletContext().getContextPath()%>/resources/js/canvasjs.min.js"></SCRIPT>
	<SCRIPT TYPE="text/javascript"
		src="<%=getServletContext().getContextPath()%>/resources/js/jquery.canvasjs.min.js"></SCRIPT>

	<script type="text/javascript"
		src="<%=getServletContext().getContextPath()%>/resources/js/utils.js"></script>

</body>
</html>
