<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- header section start-->
<div class="header-section">


	<!--search start-->
	<h2>Bienvenue dans l'assistant de gestion et de suivi des températures</h2>
	<!--search end-->


</div>
<!-- header section end-->
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"> <img alt="Brand"
				style="width: 50px;"
				src="<%=getServletContext().getContextPath()%>/resources/img/sqliLogo.png">
			</a>
		</div>

		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="<%=getServletContext().getContextPath()%>/Monitor">Monitoring</a></li>
				<li><a
					href="<%=getServletContext().getContextPath()%>/site/list">Gestion
						des sites</a></li>
				<li><a
					href="<%=getServletContext().getContextPath()%>/capteur/list">Gestion
						des capteurs</a></li>
				<li><a
					href="<c:url value="/j_spring_security_logout" />">Déconnexion</a></li>
							
			</ul>
		</div>
	</div>
</nav>