<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="fr">
<head>
<meta charset="utf-8">

<title>SQLI SENSOR</title>

<!-- Bootstrap core CSS -->
<link
	href="<%=getServletContext().getContextPath()%>/resources/css/bootstrap.min.css"
	rel="stylesheet">


<link
	href="<%=getServletContext().getContextPath()%>/resources/css/login.css"
	rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<div class="container">

		<form class="form-signin"
			action="<c:url value='j_spring_security_check' />" method="POST">
			<center><h2 class="form-signin-heading">Authentification</h2></center>
			
			<c:if test="${not empty error}">
				<div class="alert alert-warning"  role="alert"><strong id="msgAlert">${error}</strong></div>
			</c:if>
			<c:if test="${not empty message}">
				<div class="alert alert-success"  role="alert"><strong id="msgAlert">${message}</strong></div>
			</c:if>
			
			<label for="inputEmail" class="sr-only">Login</label> <input
				type="text" id="inputEmail" class="form-control"
				placeholder="Login" required="" autofocus="" name="username">
				
				
			<label for="inputPassword" class="sr-only">Mot de passe</label> <input
				type="password" id="inputPassword" class="form-control"
				placeholder="Mot de passe" required="" name="password">
				
				
			<div class="checkbox">
				<label> <input type="checkbox" value="remember-me">
					se souvenir de moi
				</label>
			</div>
			
			
			<button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>
		</form>

	</div>
	<!-- /container -->


</body>
</html>