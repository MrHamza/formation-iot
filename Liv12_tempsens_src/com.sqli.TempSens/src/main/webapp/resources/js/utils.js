var currentTemp = 0;
var ipAddrs = [];
var charts = [];
var dPoints = [];
var xVals = []
var yVal = 100;
var updateInterval = 1000;
var dataLength = 500;

var client = new Messaging.Client("sensor-oujda.sqli.com", 8000, "JsClient-"
		+ Math.floor((Math.random() * 1000) + 1));

client.onConnectionLost = function(responseObject) {
	$("#msgAlert").text("RECUPERATION DES DONNEES ARRETE ... ");
};

client.onMessageArrived = function(message) {
	updateChart(message);
};

// Connect Options
var options = {
	timeout : 3,
	// Gets Called if the connection has sucessfully been established
	onSuccess : function() {
		client.subscribe('data/temperature', {
			qos : 1
		});
		$("#msgAlert").text("RECUPERATION DES DONNEES EN COURS ... ");

	},
	// Gets Called if the connection could not be established
	onFailure : function(message) {
		$("#msgAlert").text("RECUPERATION DES DONNEES ARRETE ... ");
	}
};

/**
 * Fonction qui met à jours le graph
 * 
 */
function updateChart(message) {
	Log(message.destinationName + " | " + message.payloadString);

	tmpTab = message.payloadString.split(":");
	// tmpTab = message.split(":");
	if (ipAddrs.indexOf(tmpTab[0]) == -1) {
		ipAddrs.push(tmpTab[0]);
		createNewChart(tmpTab[0]);
		xVals[tmpTab[0]] = 0;
		initChart(tmpTab[0]);
	}

	dPoints[tmpTab[0]].push({
		x : xVals[tmpTab[0]],
		y : parseFloat(tmpTab[1])
	});
	xVals[tmpTab[0]]++;

	if (dPoints[tmpTab[0]].length > dataLength) {
		dPoints[tmpTab[0]].shift();
	}

	charts[tmpTab[0]].render();

};

/**
 * Fonction qui va logger les evenement
 * 
 */
var MAX_LOG_COUNT = 40;
var logCount = 0;
function Log(str) {
	var t = str.split(":");
	var date = new Date();
	$('#messages').prepend(
			'<tr><td>' + date.format() + '</td><td>' + t[0] + '</td><td>'
					+ t[1] + '</td></tr>');

	if (logCount > MAX_LOG_COUNT)
	{	
		$("#messages").html("");
		logCount= 0 ;
	}
	else
		logCount++;

	// console.log('<span>' + str +'</span><br/>');
}

/**
 * La fonction qui crée un nouveau graph
 * 
 */
function createNewChart(ipaddr) {
	dPoints[ipaddr] = [];

	$('#body')
			.append(
					'<div class="row"><div class="col-sm-12"><section class="panel"><header class="panel-heading">Evolution de la temperature pour le capteur '
							+ ipaddr
							+ '<span class="tools pull-right"><a href="javascript:;" class="fa fa-chevron-down"></a><a href="javascript:;" class="fa fa-times"></a></span></header><div class="panel-body"><div class="chartContainer" id="chartContainer-'
							+ ipaddr + '"></div></div></section></div></div>');

	var chart = new CanvasJS.Chart("chartContainer-" + ipaddr, {
		title : {
			text : "Evolution de la temperature pour le capteur " + ipaddr
		},
		data : [ {
			type : "line",
			dataPoints : dPoints[ipaddr]
		} ]
	});

	charts[ipaddr] = chart;
}

/**
 * Fonction qui initialise les valeurs du chart pour la premiere fois
 * 
 */
function initChart(ipaddr) {
	for (var i = 0; i < dataLength; i++) {
		dPoints[ipaddr].push({
			x : xVals[ipaddr],
			y : 0
		});
		xVals[ipaddr]++;
	}
}

/*
 * window.onload = function () {
 * 
 * 
 * var i = 0 ; // update chart after specified time. setInterval(function(){
 * 
 * updateChart(i+":"+(5+Math.random()*(-5-5))); i++ ; }, updateInterval); }
 * 
 */