			var currentTemp = 0 ; 	
			var ipAddrs = [];  
			var updateInterval = 1000;


			 var client = new Messaging.Client("10.42.6.65", 8000, "JavascriptClient");
			 
			 client.onConnectionLost = function (responseObject) {
			     alert("connexion perdu: ");
			 };
			 
			 client.onMessageArrived = function (message) {
			    updateDashboard(message);
			 };

			 //Connect Options
			 var options = {
			     timeout: 3,
			     //Gets Called if the connection has sucessfully been established
			     onSuccess: function () {
			         client.subscribe('data/temperature', {qos: 1}); 
			         alert('Connected Subscribed');

			     },
			     //Gets Called if the connection could not be established
			     onFailure: function (message) {
			         alert("Connection failed: " + message.errorMessage);
			     }
			 };

			 /**
			  * Fonction qui met à jours le dashboard  
			  *
			  */
			 function updateDashboard (message) {
				Log(message.destinationName +" | "+message.payloadString);
			     
			     tmpTab = message.payloadString.split(":"); 
			     //tmpTab = message.split(":"); 
			     if(ipAddrs.indexOf(tmpTab[0]) == -1)
			     {
			     	ipAddrs.push(tmpTab[0]);
			     	createNewZone(tmpTab[0]); 
			     }
			     
			     $('#zone-'+tmpTab[0]).html( parseFloat(tmpTab[1])+"C");
			    		

			};

			 /**
			 *  Fonction qui va logger les evenement 
			 *
			 */
			 function Log(str)
			 {
			 	$('#messages').append('<span>' + str +'</span><br/>');
			 	//console.log('<span>' + str +'</span><br/>');
			 }

			/**
	         * Tester si la temperature est urgente pour l'adresse IP passé en parametre 
	         * @param ipAddr
	         * @param temp
	         * @return 
	         */
	        function isTemperatureUrgent( ipAddr,  temp)
	        {
	            var seuil ; 
	            var t = ipAddr.split(".");
	            var indice = 0 ;  
	            
	            if(t.length == 4) 
	                indice = parseInt(t[3]);
	           
	            if(indice % 2 == 0)
	                seuil = 30;
	            else if(indice % 4 == 0 )
	                seuil = 50 ; 
	            else if(indice % 8 == 0)
	                seuil = 5; 
	            else 
	                seuil = 37; 
	            
	            return temp>seuil  ; 
	        }
			
			/**
			 * La fonction qui crée une nouvelle zone 
			 *
			 */	 
	        function createNewZone(ipaddr){
	        		
	        	$('#body').append('<div id="zone-'+ipaddr+'" class="zoneContainer"></div>');
	        	$('#zone-'+ipaddr).html(ipaddr);
	        	
	        }

	        
			/*window.onload = function () {

			
				var i = 0 ; 
			// update chart after specified time. 
			setInterval(function(){

				updateChart(i+":"+(5+Math.random()*(-5-5)));
			    i++ ; 
			}, updateInterval); 

		}

		*/