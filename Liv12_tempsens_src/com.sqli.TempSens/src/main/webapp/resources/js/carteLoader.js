$(function(){
	    
	  
	var latIng = new google.maps.LatLng(34.68472715484425,-1.9142245833587594); 
	
	var map = new google.maps.Map(document.getElementById('Gmap') , {
		zoom :  15, 
		center : latIng , 
		mapTypeId: 'satellite'
	}); 
	
	
	var marker = new google.maps.Marker({
		position : latIng , 
		map : map , 
		title : 'Bougez le marqueur', 
		draggable : true
	}) ; 
	
	
	var geocoder = new google.maps.Geocoder() ; 
	
	google.maps.event.addListener(marker , 'drag' , function(){
		setPosition(marker) ; 
	}); 
	
	google.maps.event.addListener(marker , 'dragend' , function(){
	var dim = new google.maps.LatLng($('#lat').val(),$('#lng').val());
	
		var request = {
				location : dim
			}
			geocoder.geocode(request,function(results , status){
	
			if(status == google.maps.GeocoderStatus.OK)
			{ 
				$('#adr').text(results[0].formatted_address) ;
				
			}
			});
	}); 
	
	
	
	
	$('#EventAdresse').keypress(function(e){
		if(e.keyCode == 13)
		{
			var request = {
				address : $(this).val()
			}
			geocoder.geocode(request,function(results , status){
	
			if(status == google.maps.GeocoderStatus.OK)
			{
				var pos = results[0].geometry.location ; 
				$('#adr').val(results[0].formatted_address) ;
				map.setCenter(pos);
				marker.setPosition(pos); 
				setPosition(marker);
	
			}
		});
			return false  ; 
		}
	});
	
	function setPosition(marker){
		var pos = marker.getPosition();
		 $('#lat').val(pos.lat()) ; 
		  $('#lng').val(pos.lng()) ; 
		  
	}
	
	});