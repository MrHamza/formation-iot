En attente de la mise en place d'un fichier de configuration .properties, 
Veuillez configurer l'application comme suit: 

Parametres de la base de données 
================================
ouvrez le fichier Liv12_tempsens_src\com.sqli.TempSens\src\main\webapp\WEB-INF\spring\MongoConfig.xml 
* Si mongodb est executé sur un serveur autre que celui ou tomcat est executé changez 127.0.0.1 avec l'@ IP du serveur 

Parametres du broker MQTT  
================================
ouvrez le fichier Liv12_tempsens_src\com.sqli.TempSens\src\main\webapp\WEB-INF\spring\root-context.xml 
* Si le broker est executé sur un serveur autre que celui ou tomcat est executé changez 127.0.0.1 (brokerAddr) avec l'@ IP du serveur 

Parametres d'authentification 
===============================
Dans la nouvelle version : La mise en place d'un système d'authentification en utilisant Spring Security 
Login : admin 
Mot de passe : pass-147
