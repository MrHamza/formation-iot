
package com.sqli.raspberryInformationsCollector;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 *
 * @author HSAIN
 */
public class MQTTRaspberryClient extends Thread implements MqttCallback{
    public static String ID = "SIMULATEUR =>";
    private MqttClient client ;
    private String brokerAddr ;  
    // les sockets:
    private java.net.ServerSocket serveur;
    private ArrayList<ThreadClient> connexions = null;

    public MQTTRaspberryClient(String broker)
    {
        brokerAddr = broker ; 
        connexions = new ArrayList<>();
    }
    
    @Override
    public void run() 
    {
        
        int qos             = 1;
        String broker       = "tcp://"+brokerAddr+":1883";
        String clientId     = "RaspberryClientSimulateur";
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            client = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            System.out.println(ID+"Connexion au broker: "+broker);
            client.connect(connOpts);
            System.out.println(ID+"Connected");
            
            client.setCallback(this);
            serveur = new ServerSocket(5001,100);
            
            while(true)
            {
            	attendreUneConnexion();
            }
            
           
        } catch(Exception me) {
            
            System.out.println(ID+"cause "+me.getCause());
            System.out.println(ID+"excep "+me);
        }
    }
    @Override
    public void connectionLost(Throwable thrwbl) {
        System.out.println(ID+"Connection lost");
    }

    @Override
    public void messageArrived(String string, MqttMessage mm) throws Exception {
       
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken imdt) {
        System.out.println(ID+"delivery Complete");
    }
     
     private void attendreUneConnexion()throws IOException{
            System.out.println(ID+"En attente de connexion d'un client......\n");
            //Faire en sorte que lengthserveur accepte une connexion.
            Socket con = serveur.accept();
            ThreadClient tc = new ThreadClient(con,client) ; 
            connexions.add(tc); 
            tc.start();
       }
}
