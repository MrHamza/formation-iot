
package com.sqli.raspberryInformationsCollector;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
    * @author HSAIN
 */
public class MainProgramm {
    
    public static void main(String[] args)
    {
         if(args.length == 1)
        {
             MQTTRaspberryClient simulateurClient = new MQTTRaspberryClient(args[0]);
             TemperatureSender sender = new TemperatureSender(args[0]);
             try {
                 simulateurClient.start();
                 sender.lancerCient();
             } catch (IOException ex) {
                 Logger.getLogger(MainProgramm.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
        else
        {
            System.out.println("Syntax : program <Broker IP Addr>");
        }
    }
}
