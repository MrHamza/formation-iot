
package com.sqli.raspberryInformationsCollector.utils;

/**
 *
 * @author HSAIN
 */
public enum PhysicalQuantity {
    Temperature, Humidity
}
