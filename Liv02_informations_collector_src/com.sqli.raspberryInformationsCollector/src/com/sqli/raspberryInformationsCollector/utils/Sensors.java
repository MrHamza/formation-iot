/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.raspberryInformationsCollector.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author HSAIN
 */
public class Sensors {
	/**
	 * @return all available sensors.
	 * @throws IOException
	 */
	public static Set<Sensor> getSensors() throws IOException {
		Set<Sensor> sensors = new HashSet<Sensor>();
		addDallasSensors(sensors);
		return sensors;
	}

	
	private static void addDallasSensors(Set<Sensor> sensors)
			throws IOException {
		File sensorFolder = new File("/sys/bus/w1/devices");
		if (!sensorFolder.exists()) {
			throw new IOException("Impossible de trouver les periph. W1! Soyez sur que w1-gpio et w1-therm sont chargés.");
		}
		for (File f : sensorFolder.listFiles()) {
			if (f.getName().startsWith("w1_bus_master")) {
				continue;
			}
			sensors.add(new DallasSensorDS18B20(f));
		}
	}
}
