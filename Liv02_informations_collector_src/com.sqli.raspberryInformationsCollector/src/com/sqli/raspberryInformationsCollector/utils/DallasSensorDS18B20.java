
package com.sqli.raspberryInformationsCollector.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * DS18B20 type temperature sensor.
 * 
 * @author HSAIN
 */
public class DallasSensorDS18B20 implements Sensor {
	private final File sensorFile;
	private final File valueFile;

	public DallasSensorDS18B20(File sensorFile) {
		this.sensorFile = sensorFile;
		this.valueFile = deriveValueFile(sensorFile);
	}

	@Override
	public String getID() {
		return sensorFile.getName();
	}

	@Override
	public Number getValue() throws IOException {
		try (BufferedReader reader = new BufferedReader(new FileReader(
				valueFile))) {
			String tmp = reader.readLine();
			int index = -1;
			while (tmp != null) {
				index = tmp.indexOf("t=");
				if (index >= 0) {
					break;
				}
				tmp = reader.readLine();
			}
			if (index < 0) {
				throw new IOException("Impossible de lire depuis le capteur " + getID());
			}
			return Integer.parseInt(tmp.substring(index + 2)) / 1000f;
		}
	}

	private static File deriveValueFile(File sensorFile) {
		return new File(sensorFile, "w1_slave");
	}
	
	public String toString() {
		try {
			return String.format("Sensor ID: %s, Temperature: %2.3fC", getID(), getValue());
		} catch (IOException e) {
			return String.format("Sensor ID: %s - could not read temperature!C", getID());
		}
	}

	@Override
	public PhysicalQuantity getPhysicalQuantity() {
		return PhysicalQuantity.Temperature;
	}

	@Override
	public String getUnitString() {
		return "\u00B0C";
	}
}
