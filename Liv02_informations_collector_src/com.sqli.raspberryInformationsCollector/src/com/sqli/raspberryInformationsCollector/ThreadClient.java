package com.sqli.raspberryInformationsCollector;

import java.io.ObjectInputStream;
import java.net.Socket;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class ThreadClient extends Thread {

    private Socket connexion;
    private java.io.ObjectInputStream entree;
    private MqttClient client;

    public ThreadClient(Socket con, MqttClient cli) {

        this.connexion = con;
        this.client = cli;
    }

    public void run() {
        try {
            System.out.println(MQTTRaspberryClient.ID + "Recuperation de l'inputStream");

            entree = new ObjectInputStream(connexion.getInputStream());

            System.out.println(MQTTRaspberryClient.ID + "Nouveau capteur connecté " + connexion.getInetAddress().getHostAddress());

            float temperature = 0;
            String topic = "data/temperature";

            String content = "";
            while (true) {
                temperature = entree.readFloat();
                content = connexion.getInetAddress().getHostAddress() + ":" + temperature;
                System.out.println(MQTTRaspberryClient.ID + "Publication de la temperature: " + content);
                MqttMessage message = new MqttMessage(content.getBytes());
                message.setQos(1);
                client.publish(topic, message);

                System.out.println(MQTTRaspberryClient.ID + "Temperature publiee");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
