package com.sqli.raspberryInformationsCollector;

import java.io.IOException;
import java.util.Set;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.sqli.raspberryInformationsCollector.utils.Sensor;
import com.sqli.raspberryInformationsCollector.utils.Sensors;

/**
 *
 * @author HSAIN
 */
public class TemperatureSender implements MqttCallback {

	private static String ID = "CAPTEUR =>";
	private MqttClient client;
	private String brokerAddr;

	// les sockets:

	public TemperatureSender(String broker) {
		brokerAddr = broker;
	}

	public void lancerCient() throws IOException {

		int qos = 1;
		String broker = "tcp://" + brokerAddr + ":1883";
		String clientId = "RaspberryClientCapteur";
		MemoryPersistence persistence = new MemoryPersistence();

		try {
			client = new MqttClient(broker, clientId, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true);
			System.out.println(ID + "Connexion au broker: " + broker);
			client.connect(connOpts);
			System.out.println(ID + "Connected");
			client.setCallback(this);
			float temperature;
			String topic = "data/temperature";
			String content;
			Set<Sensor> tempSens = Sensors.getSensors();
			Sensor sensor = null;
			System.out.println(ID + "Nombre de capteurs detectés "
					+ tempSens.size());
			while (!tempSens.isEmpty()) {

				/*
				 * System.out.println(ID+String.format("%s(%s):%3.2f%s",
				 * sensor.getPhysicalQuantity(), sensor.getID(),
				 * sensor.getValue(), sensor.getUnitString()));
				 */
				for (Sensor tempSen : tempSens) {
					sensor = tempSen;
					temperature = (float) sensor.getValue();
					content = sensor.getID() + ":" + temperature;
					System.out.println(ID + "Publication de la temperature: "
							+ content);
					MqttMessage message = new MqttMessage(content.getBytes());
					message.setQos(qos);
					client.publish(topic, message);
					Thread.sleep(500);
				}

				Thread.sleep(500);
				System.out.println("");
				System.out.flush();
			}

		} catch (Exception me) {

			System.out.println("Cause " + me.getCause());
			System.out.println("Excep " + me);
			me.printStackTrace();
		}
	}

	@Override
	public void connectionLost(Throwable thrwbl) {
		System.out.println(ID + "Connection perdue");
	}

	@Override
	public void messageArrived(String string, MqttMessage mm) throws Exception {

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken imdt) {
		System.out.println(ID + "Message delivré");
	}

}
